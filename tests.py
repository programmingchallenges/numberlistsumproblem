import unittest
import main

class TestStringMethods(unittest.TestCase):

    def testBasic(self):
        self.assertEqual(main.doAnyNumbersInListAddToTargetNumber([10, 15, 3, 7], 17), True)

    def testFailingSum(self):
        self.assertEqual(main.doAnyNumbersInListAddToTargetNumber([1,2,3,4,5,6,7,8,18,19,20,21,22,23,24,25,26,27,28,29,30], 17), False)

    def testNegativeSum(self):
        self.assertEqual(main.doAnyNumbersInListAddToTargetNumber([-5,22, 1, 2, 3, 4, 5], 17), True)

    def test0Sum(self):
        self.assertEqual(main.doAnyNumbersInListAddToTargetNumber([0,17, 1, 2, 3, 4, 5], 17), True)

    def testSingleNumberMatch(self):
        self.assertEqual(main.doAnyNumbersInListAddToTargetNumber([17], 17), False)


if __name__ == '__main__':
    unittest.main()