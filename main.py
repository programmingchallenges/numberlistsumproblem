import sys


def main(numberList, targetNumber):
   print(doAnyNumbersInListAddToTargetNumber(numberList, targetNumber)) 
   

def doAnyNumbersInListAddToTargetNumber(numberList, targetNumber):
    numbersThatAddToTargetNumber = {}
    for number in numberList:
        if number in numbersThatAddToTargetNumber:
            return True
        else:
            numbersThatAddToTargetNumber[targetNumber - number] = ''
    return False

if __name__ == '__main__':
    main(sys.argv[1], sys.argv[2])